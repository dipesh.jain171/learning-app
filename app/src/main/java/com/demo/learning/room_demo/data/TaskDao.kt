package com.demo.learning.room_demo.data

import androidx.room.*

@Dao
interface TaskDao {
    @Query("Select * from task")
    fun getAllTask(): List<TaskEntity>

    @Insert
    fun insert(taskEntity: TaskEntity)

    @Update
    fun update(taskEntity: TaskEntity)

    @Delete
    fun delete(taskEntity: TaskEntity)
}