package com.demo.learning.room_demo

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.demo.learning.R
import com.demo.learning.room_demo.data.AppDatabase
import com.demo.learning.room_demo.data.TaskEntity

class RoomDemoFragment : Fragment(R.layout.fragment_room_demo) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Thread{
            val taskDao = AppDatabase.invoke(context!!).taskDao()
            taskDao.insert(TaskEntity())
            val count = taskDao.getAllTask().size
            activity!!.runOnUiThread{
                Toast.makeText(context!!,"Total count $count",Toast.LENGTH_SHORT).show()
            }

        }.start()
    }
}