package com.demo.learning.recyclerview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.learning.R
import kotlinx.android.synthetic.main.fragment_recyclerview.*

class RecyclerViewDemo :Fragment(R.layout.fragment_recyclerview) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerViewDemo.layoutManager=LinearLayoutManager(context)
        var list=Array<ItemList>(5){ItemList(6,"Topic List")}
        var listt= ArrayList<ItemList>()
        for(intt in 0..25){
           listt.add( ItemList(6,"Topic List $intt"))
        }
        recyclerViewDemo.adapter=RecyclerViewAdapter(context!!,listt)
    }
}