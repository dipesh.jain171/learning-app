package com.demo.learning.recyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.demo.learning.R
import kotlinx.android.synthetic.main.itemview_recyclerview.view.*

class RecyclerViewAdapter(
    val context: Context,
    var itemList: ArrayList<ItemList>
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val topic_name_textview = itemView.topic_name_textview;
        fun setCustomView(iTemList: ItemList) {
            topic_name_textview.text = iTemList.itemName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.itemview_recyclerview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setCustomView(itemList.get(position))
    }

    override fun getItemCount(): Int = itemList.size
}