package com.demo.learning.topics

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.learning.MainActivity
import com.demo.learning.R
import kotlinx.android.synthetic.main.fragment_topics_list.*

class TopicsFragment : Fragment(R.layout.fragment_topics_list) {
    lateinit var topicsAdapter: TopicsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        topic_recycler_view.layoutManager = LinearLayoutManager(context)
        val iTopicClickListener: TopicsAdapter.ITopicClickListener = object :
            TopicsAdapter.ITopicClickListener {
            override fun onClickTopic(position: Int, topic_name: String) {
                Toast.makeText(context, topic_name, Toast.LENGTH_LONG).show()
                when (topic_name) {
                    "Recycler View" -> {
                        val intent = Intent(context, MainActivity::class.java)
                        startActivity(intent)
                    }
                    "Room" -> {
                    }
                }
            }

        }

        topicsAdapter =
            TopicsAdapter(
                context!!,
                itemList = arrayOf(
                    "Recycler View",
                    "Room",
                    "Paging",
                    "SQLite",
                    "View Model",
                    "Work Manager",
                    "Memory Leak",
                    "Firebase",
                    "Play core"
                ),
                iTopicClickListener
            )
        topic_recycler_view.adapter = topicsAdapter
    }
}