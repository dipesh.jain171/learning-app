package com.demo.learning.topics

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.learning.R
import kotlinx.android.synthetic.main.itemview_topic.view.*

class TopicsAdapter(
    val context: Context,
    var itemList: Array<String>,
    val iTopicClickListener : ITopicClickListener
) : RecyclerView.Adapter<TopicsAdapter.ViewHolder>() {
    interface ITopicClickListener{
        fun onClickTopic(position: Int,topic_name:String)
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var topic_name_textview: AppCompatTextView
        init {
            topic_name_textview=itemView.topic_name_textview
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ViewHolder(
            (LayoutInflater.from(context)).inflate(
                R.layout.itemview_topic,
                parent,
                false
            )
        )
        return view;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.topic_name_textview.text = itemList.get(position)
    holder.itemView.setOnClickListener {
        iTopicClickListener.onClickTopic(position,itemList.get(position))
    }
    }

    override fun getItemCount(): Int = itemList.size
}