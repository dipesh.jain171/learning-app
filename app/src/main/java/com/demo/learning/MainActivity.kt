package com.demo.learning

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.learning.topics.TopicsAdapter
import com.demo.learning.topics.TopicsAdapter.ITopicClickListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var newController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        newController = Navigation.findNavController(this,R.id.fragment)
        NavigationUI.setupWithNavController(navigation_menu,newController)
        NavigationUI.setupActionBarWithNavController(this,newController,drawer)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp( newController,drawer)
    }
}